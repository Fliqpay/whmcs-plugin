<?php
/**
 * WHMCS Sample Payment Gateway Module
 *
 * Payment Gateway modules allow you to integrate payment solutions with the
 * WHMCS platform.
 *
 * This sample file demonstrates how a payment gateway module for WHMCS should
 * be structured and all supported functionality it can contain.
 *
 * Within the module itself, all functions must be prefixed with the module
 * filename, followed by an underscore, and then the function name. For this
 * example file, the filename is "gatewaymodule" and therefore all functions
 * begin "gatewaymodule_".
 *
 * If your module or third party API does not support a given function, you
 * should not define that function within your module. Only the _config
 * function is required.
 *
 * For more information, please refer to the online documentation.
 *
 * @see https://developers.whmcs.com/payment-gateways/
 *
 * @copyright Copyright (c) WHMCS Limited 2017
 * @license http://www.whmcs.com/license/ WHMCS Eula
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

#create the transaction table
use WHMCS\Database\Capsule;

// Create a new table.
try {
    Capsule::schema()->create(
        '_fliqpay_checkout_transactions',
        function ($table) {
            /** @var \Illuminate\Database\Schema\Blueprint $table */
            $table->increments('id');
            $table->integer('order_id');
            $table->string('transaction_id');
            $table->string('transaction_status');
            $table->timestamps();
        }
    );
} catch (\Exception $e) {
    #echo "Unable to create my_table: {$e->getMessage()}";
}


/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related capabilities and
 * settings.
 *
 * @see https://developers.whmcs.com/payment-gateways/meta-data-params/
 *
 * @return array
 */
function fliqpaycheckout_MetaData()
{
    return array(
        'DisplayName' => 'Fliqpay_Checkout_WHCMS',
        'APIVersion' => '1.1', // Use API Version 1.1
        'DisableLocalCredtCardInput' => true,
        'TokenisedStorage' => false,
    );
}

/**
 * Define gateway configuration options.
 *
 * The fields you define here determine the configuration options that are
 * presented to administrator users when activating and configuring your
 * payment gateway module for use.
 *
 * Supported field types include:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each field type and their possible configuration parameters are
 * provided in the sample function below.
 *
 * @return array
 */
function fliqpaycheckout_config()
{
    return array(
        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'Fliqpay Checkout',
        ),
        // a text field type allows for single line text input
        'business_key_dev' => array(
            'FriendlyName' => 'Development Key',
            'Type' => 'text',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Your <b>development</b> business key.  <a href = "http://sandbox.fliqpay.com/dashboard" target = "_blank">Please check the top right corner of your dashboard</a> and copy it`e.g B-xxxxxxxx`.',
        ),
        // a text field type allows for single line text input
        'business_key_prod' => array(
            'FriendlyName' => 'Production Key',
            'Type' => 'text',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Your <b>production</b> business key.  <a href = "https://app.fliqpay.com/dashboard" target = "_blank">Please check the top right corner of your dashboard</a> and copy it`e.g B-xxxxxxxx`.',
        ),
       
        'fliqpay_checkout_environment' => array(
            'FriendlyName' => 'Environment',
            'Type' => 'dropdown',
            'Options' => 'Test,Production',
            'Description' => 'Select <b>Test</b> for testing the plugin, <b>Production</b> when you are ready to go live.<br>',
        ),
    );
}

/**
 * Payment link.
 *
 * Required by third party payment gateway modules only.
 *
 * Defines the HTML output displayed on an invoice. Typically consists of an
 * HTML form that will take the user to the payment gateway endpoint.
 *
 * @param array $config_params Payment Gateway Module Parameters
 *
 * @see https://developers.whmcs.com/payment-gateways/third-party-gateway/
 *
 * @return string
 */
function gatewaymodule_link($params)
{
    // Gateway Configuration Parameters
    // $accountId = $params['accountID'];
    // $secretKey = $params['secretKey'];
    // $testMode = $params['testMode'];
    // $dropdownField = $params['dropdownField'];
    // $radioField = $params['radioField'];
    // $textareaField = $params['textareaField'];

    // Invoice Parameters
    $invoiceId = $params['invoiceid'];
    $description = $params["description"];
    $amount = $params['amount'];
    // $currencyCode = $params['currency'];

    // Client Parameters
    $customerName = $params['clientdetails']['firstname'] . ' ' . $params['clientdetails']['lastname'];
    $customerEmail = $params['clientdetails']['email'];
    // $address1 = $params['clientdetails']['address1'];
    // $address2 = $params['clientdetails']['address2'];
    // $city = $params['clientdetails']['city'];
    // $state = $params['clientdetails']['state'];
    // $postcode = $params['clientdetails']['postcode'];
    // $country = $params['clientdetails']['country'];
    // $phone = $params['clientdetails']['phonenumber'];

    // System Parameters
    $systemUrl = $params['systemurl'];
    $langPayNow = $params['langpaynow'];
    $moduleDisplayName = $config_params['name'];
    $moduleName = $config_params['paymentmethod'];
    $returnUrl = $params['returnurl'];

    // $companyName = $params['companyname'];
    // $whmcsVersion = $params['whmcsVersion'];

    $fliqpayConfig = switchEnvironment($params);
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
    $dir = dirname($_SERVER['REQUEST_URI']);
    if ($dir == '/') {
        $dir = '';
    }
    $postfields = array();
    $postfields['description'] = $description;
    $postfields['amount'] = $amount;
    $postfields['customerName'] = $customerName;
    $postfields['customerEmail'] = $customerEmail;
    $postfields['businessKey'] = $fliqpayConfig->businessKey;
    $postfields['isAmountFixed'] = true;
    $postfields['useCurrenciesInWalletSettings'] = true;
    $postfields['name'] = "";
    $postfields['acceptedCurrencies'] = "";
    $postfields['orderId'] = $invoiceId;
    $postfields['returnUrl'] = $returnUrl;
    $postfields['callbackURL'] = $protocol . $_SERVER['SERVER_NAME'] . $dir . '/modules/gateways/callback/fliqpaycheckout_callback.php';


    $htmlOutput = '<form method="POST" action="' . $fliqpayConfig->url . '">';
    foreach ($postfields as $k => $v) {
        $htmlOutput .= '<input type="hidden" name="' . $k . '" value="' . urlencode($v) . '" />';
    }
    $htmlOutput .= '<input type="submit" value="' . $langPayNow . '" />';
    $htmlOutput .= '</form>';

    #insert into the database
    $pdo = Capsule::connection()->getPdo();
    $pdo->beginTransaction();
    
    $created_at = 'Y-m-d';
    


    try {
        $statement = $pdo->prepare(
            'insert into _fliqpay_checkout_transactions (order_id, transaction_id, transaction_status,created_at) values (:order_id, :transaction_id, :transaction_status,:created_at)'
        );

        $statement->execute(
            [
                ':order_id' => $params->orderId,
                ':transaction_id' => "",
                ':transaction_status' => 'pending',
                ':paymentAmount' => 0,
                ':created_at' => date($created_at.' H:i:s'),
            ]
        );
        $pdo->commit();
    } catch (\Exception $e) {
        error_log($e->getMessage());
        $pdo->rollBack();
    }

    return $htmlOutput;
}


/**
 * handle switch between test and production environment.
 *
 * @param string $params Payment Gateway Module Parameters
 *
 * @return object
 */
function switchEnvironment($params)
{
    $fliqpayConfig = new stdClass();
    if ($params['fliqpay_checkout_environment'] == "Test") {
        $fliqpayConfig->environment = "test";
        $fliqpayConfig->url = "http://sandboxapi.fliqpay.com/i/paymentButton";
        $fliqpayConfig->businessKey = $params['business_key_dev'];
    }
    if ($params['fliqpay_checkout_environment'] == "Production") {
        $fliqpayConfig->environment = "production";
        $fliqpayConfig->url = "https://api.fliqpay.com/i/paymentButton";
        $fliqpayConfig->businessKey = $params['business_key_prod'];
    }
    return $fliqpayConfig;
}
